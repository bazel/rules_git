# `rules_git`

> A hermetic Bazel ruleset for working with `git` repositories.

## Getting Started

Add the following to `MODULE.bazel`:

```py
bazel_dep(name="rules_git", version="0.0.0")
```

## Usage

Add a fetch of a repository to `MODULE.bazel`:

```py
fetch = use_repo_rule("@rules_git//git/fetch:defs.bzl", "git_fetch")

fetch(
    name = "github-git-2.43.0",
    reference = "refs/tags/v2.43.0",
    remote = "https://github.com/git/git.git",
)
```

`.git` objects will be fetched into a repository.

The repository has targets to perform `git` actions on the `.git` objects.

The most useful being `checkout` which provides the `git` repository working tree as a declared directory (`TreeArtifact`):

```py
load("@bazel_skylib//rules:build_test.bzl", "build_test")

build_test(
    name = "checkout",
    size = "small",
    targets = [
        "@github-git-2.43.0//:checkout",
    ],
)
```

List the available targets with `bazel query "@github-git-2.43.0//*"`

## Hermeticity

The ruleset is entirely hermetic and is powered via bazel/git>.
