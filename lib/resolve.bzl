load(":execute.bzl", "execute")
load(":init.bzl", _ATTRS = "ATTRS")

visibility("//git/...")

ATTRS = {
    "remote": attr.string(
        doc = "The URI to retrieve `git` objects from.",
        mandatory = True,
    ),
    "reference": attr.string(
        doc = "A reference to resolve.",
    ),
    "git": _ATTRS["git"],
}

def resolve(rctx, *, remote = None, commit = None, reference = None, git = None):
    """
    Resolves a `git` reference into a commit SHA.

    All arguments default to the same named attributes on `rctx.`

    Args:
      rctx: The repository context to use for command execution.
      remote: The `git` remote endpoint that contains the reference.
      commit: The commit SHA.
      reference: The reference to resolve.
      git: The path to the `git` CLI.

    Returns:
      The fully resolved `git` commit SHA and the resolved reference.
    """
    remote = remote or rctx.attr.remote
    commit = commit or rctx.attr.commit
    reference = reference or rctx.attr.reference
    git = git or rctx.attr.git

    if len(commit) == 40 and commit.isalnum():
        return commit, reference

    if not reference:
        reference = ("refs/heads/main", "refs/heads/master", "refs/heads/trunk")
    elif type(reference) == "string":
        reference = (reference,)

    result = execute(rctx, "ls-remote", "--exit-code", remote, git = git, *reference)
    if result.return_code != 0:
        fail("Failed to resolve `{}#{}`: {}".format(remote, ",".join(reference), result.stderr))

    tuples = [l.split("\t") for l in result.stdout.strip().splitlines()]
    if len(tuples) > len(reference):
        fail("Ambiguous reference `{}` for {}. Multiple results were found: {}".format(remote, ", ".join(reference), ", ".join([r for _, r in tuples])))

    commit, resolved = tuples[0]

    if len(commit) != 40 and commit.isalnum():
        fail("Resolved `git` reference `{}#{}` was no a valid commit SHA: {}".format(remote, reference, commit))

    return commit, resolved
