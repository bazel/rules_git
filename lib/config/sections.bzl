load(":tokens.bzl", _tokens = "tokens")

visibility("//lib/...")

def _pairs(tokens):
    """
    Finds all consecutive "pair" tokens.

    Args:
      tokens: The iterable of configuration tokens.

    Returns:
      A tuple of the pair tokens and the rest of the tokens
    """
    pairs = []
    for i in range(0x7eadc0de):
        if i >= len(tokens):
            break

        token = tokens[i]
        if token.kind != "pair":
            return pairs, tokens[i:]

        pair = (token.key, token.value)

        pairs.append(pair)

    return tuple(pairs), ()

def _section(tokens):
    """
    Processes a section token.

    Args:
      tokens: The iterable of configuration tokens.

    Returns:
      A tuple of the section and the rest of the tokens
    """
    token = tokens[0]
    if token.kind != "section":
        fail("Expected `section` token: {}".format(token))

    pairs, tokens = _pairs(tokens[1:])

    section = struct(
        name = token.name,
        quoted = token.quoted,
        pairs = pairs,
    )

    return section, tokens

def sections(raw):
    """
    Processes a configuration string into section structures.

    Args:
      raw: The configuation string to parse.

    Returns:
      A collection of the section structures.
    """
    tokens, _ = _tokens(raw, optional = True)
    if not tokens:
        return ()

    collection = []
    for _ in range(0x7eadc0de):
        if not tokens:
            break
        section, tokens = _section(tokens)
        collection.append(section)
    return tuple(collection)
