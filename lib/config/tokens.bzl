load("@bazel_skylib//lib:structs.bzl", "structs")

visibility("//lib/...")

def _next(raw, character, *, optional = False):
    """
    Searches for the next character.

    Args:
      raw: The current raw string
      character: The character to find in the `raw` string
      optional: Do not raise failure if not found.

    Returns:
      A tuple of character found and the processed raw string
    """
    prefix, c, suffix = raw.partition(character)
    if not c:
        if optional:
            return None, raw
        if prefix and not prefix.isspace():
            fail("`{}` was not the next character in the configuration string: {}".format(character, raw))
        fail("Failed to find `{}` in the configuration string: {}".format(character, raw))
    return c, suffix

def _consume(raw, predicate, *, optional = False):
    """
    Consumes allowed characters from the raw string until one is not found.

    Args:
      raw: The current raw string.
      predicate: A function that determines if a character is allowed
      optional: Do not raise failure if not found.

    Returns:
      A tuple of the consumed characters and the processed raw string
    """
    for i in range(len(raw)):
        if not predicate(raw[i]):
            return raw[:i], raw[i:]
    return raw, ""

def _quoted(raw, *, optional = False):
    """
    Searches for a double quoted string.

    Args:
      raw: The current raw string.
      optional: Do not raise failure if not found.

    Returns:
      A tuple of character found and the processed raw string
    """
    found, raw = _next(raw, '"', optional = True)
    if not found:
        if optional:
            return None, raw
        fail("Failed to find quoted string in the configuration string: {}".format(raw))

    prefix, c, suffix = raw.partition('"')
    if not c:
        fail("Failed to find end of quoted string in the configuration string: {}".format(raw))
    return prefix, suffix

def _section(raw, *, optional = False):
    """
    Searches for a section header.

    Args:
      raw: The current raw string
      optional: Do not raise failure if not found.

    Returns:
      A tuple of the section token structure and the processed raw string
    """
    found, raw = _next(raw, "[", optional = optional)
    if optional and not found:
        return None, raw
    section, raw = _consume(raw, lambda x: x.isalpha())
    quoted, raw = _quoted(raw, optional = True)
    _, raw = _next(raw, "]")
    _, raw = _next(raw, "\n")
    return struct(
        kind = "section",
        name = section,
        quoted = quoted,
    ), raw

def _pair(raw, *, optional = False):
    """
    Searches for a key/value pair.

    Args:
      raw: The current raw string
      optional: Do not raise failure if not found.

    Returns:
      A tuple of the pair token structure and the processed raw string
    """
    prefix, section, _ = raw.partition("[")
    if (not prefix or prefix.isspace()) and section == "[":
        return None, raw

    key, found, raw = raw.partition("=")
    if not found:
        if optional:
            return None, raw
        fail("Failed to find key/value pair in the configuration string: {}".format(raw))

    value, _, raw = raw.partition("\n")
    for _ in range(0x7eadc0de):
        length = len(value)
        if 0 == (length - len(value.rstrip("\\"))) % 2:
            break
        if value[len(value)] != "\\":
            break
        prefix, newline, raw = raw.partition("\n")
        value += prefix
        if not newline:
            break

    return struct(kind = "pair", key = key.strip(), value = value.lstrip()), raw

def _pairs(raw, *, optional = False):
    """
    Searches for all key/value pairs until the next section.

    Args:
      raw: The current raw string
      optional: Do not raise failure if not found.

    Returns:
      A tuple of pair tokens and the processed raw string
    """
    pairs = []
    for _ in range(0x7eadc0de):
        pair, raw = _pair(raw, optional = True)
        if not pair:
            break
        pairs.append(pair)

    if not optional and not pairs:
        fail("Failed to find key/value pairs in configuration string: {}".format(raw))

    return tuple(pairs), raw

def tokens(raw, *, optional = False):
    """
    Retrieves all tokens in a configuration string.

    Args:
      raw: The current raw string
      optional: Do not raise failure if not found.

    Returns:
      A tuple of the tokens and the processed raw string
    """
    tokens = []
    for _ in range(0x7eadc0de):
        section, raw = _section(raw, optional = optional)
        if not section:
            break
        tokens.append(section)

        pairs, raw = _pairs(raw)
        tokens.extend(pairs)

        if not raw:
            break

    if not optional and not tokens:
        fail("Failed to find any tokens in configuration string: {}".format(raw))

    return tuple(tokens), raw
