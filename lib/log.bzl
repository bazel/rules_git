visibility("//git/...")

ERROR = "error"
WARNING = "warning"
INFO = "info"
DEBUG = "debug"
VERBOSE = "verbose"

LEVEL = struct(
    error = ERROR,
    warning = WARNING,
    info = INFO,
    debug = DEBUG,
    verbose = VERBOSE,
)

def filters(rctx):
    """
    Determines the current filters.

    Args:
      rctx: The repository context to use for command execution.

    Returns:
      A mapping of modules to logging levels.
    """
    # TODO: it would be nice to cache the logging levels

    map = {}

    tags = rctx.getenv("BAZEL_RULES_GIT_LOG_TAGS", "*:E")
    for tag in tags.split(" "):
        module, colon, level = tag.strip().partition(":")
        if not colon:
            continue
        if level == "V":
            map[module] = (ERROR, WARNING, INFO, DEBUG, VERBOSE)
        elif level == "D":
            map[module] = (ERROR, WARNING, INFO, DEBUG)
        elif level == "I":
            map[module] = (ERROR, WARNING, INFO)
        elif level == "W":
            map[module] = (ERROR, WARNING)
        elif level == "E":
            map[module] = (ERROR,)
        map[module] = {k: None for k in map[module]}

    return map

def filter(rctx, module, level):
    """
    Determines if a logging message should be filtered according to what the user has requested.

    Args:
      rctx: The repository context to use for command execution.
      module: The module the message is part of.
      level: The logging level

    Returns:
      `True` if the message should be output.
    """
    map = filters(rctx)
    return level in map.get(module, map.get("*", ()))

def log(rctx, module, level, msg, *k, **kw):
    """
    Outputs a warning if the user has requested it in the `BAZEL_RULES_GIT_WARNING` environment variable.

    Args:
      rctx: The repository context to use for command execution.
      module: The module the message is part of.
      level: The logging level.
      msg: The warning message to output.

    Returns:
      `True` if the message was output
    """
    if filter(rctx, module, level):
        print("{}: {}".format(level, msg.format(*k, **kw)))
        return True
    return False
