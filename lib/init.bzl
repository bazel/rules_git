visibility("//git/...")

ATTRS = {
    "dir": attr.label(
        doc = "The `git` objects directory.",
    ),
    "bare": attr.bool(
        doc = "Initialize a bare repository.",
        default = False,
    ),
    "git": attr.label(
        doc = "The `git` CLI",
        default = "@git//:entrypoint",
        allow_single_file = True,
        executable = True,
        cfg = "exec",
    ),
}

def init(rctx, *, dir = None, git = None, bare = False):
    """
    Initializes a `git` directory for storing `git` objects.

    Can be used with the `--git-dir` argument.

    All arguments default to the same named attributes on `rctx.`

    Args:
      rctx: The repository context to use for command execution.
      dir: The `git` directory to initialize.
      git: The path to the `git` CLI.

    Returns:
      The create `git` directory.
    """
    dir = dir or rctx.attr.dir or rctx.path(".git")
    git = git or rctx.attr.git
    bare = git or rctx.attr.bare

    dir = rctx.path(dir)

    if dir.basename == "WORKSPACE":
        dir = dir.dirname

    cmd = (
        git,
        "--git-dir={}".format(dir),
        "init",
    )

    if bare:
        cmd += ("--bare",)

    rctx.report_progress("init: {}".format(dir))
    result = rctx.execute(cmd)
    if result.return_code != 0:
        fail("Failed to initialize `{}`: {}".format(dir, result.stderr))

    cmd = (
        git,
        "--git-dir={}".format(dir),
        "config",
        "core.compression",
        "9",
    )

    rctx.report_progress("config: {}".format(dir))
    result = rctx.execute(cmd)
    if result.return_code != 0:
        fail("Failed to configure `{}`: {}".format(dir, result.stderr))

    return dir
