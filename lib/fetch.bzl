load("@bazel_skylib//lib:structs.bzl", "structs")
load(":execute.bzl", "execute")
load(":submodules.bzl", "submodules")
load(":init.bzl", "init", _ATTRS = "ATTRS")
load(":log.bzl", "LEVEL", "log")

visibility("//git/...")

ATTRS = _ATTRS | {
    "remote": attr.string(
        doc = "The URI to retrieve `git` objects from.",
        mandatory = True,
    ),
    "commit": attr.string(
        doc = "The wanted commit SHA.",
    ),
    "reference": attr.string(
        doc = "A reference that contains the commit.",
    ),
    "recursive": attr.bool(
        doc = "Fetch all nested submodules.",
        default = True,
    ),
}

def _guess(rctx, remote, commit, reference, *, dir, git, guesses = ("refs/heads/main", "refs/heads/master", "refs/heads/trunk")):
    """
    Guesses the default reference for a repository, if no reference is provided.

    Args:
      rctx: The repository context to use for command execution.
      remote: The `git` remote endpoint to receive objects from.
      commit: The commit to find in any advertised reference.
      reference: A (optional) reference that contains the commit SHA.
      dir: The `git` directory to store the object into.
      git: The path to the `git` CLI.

    Returns:
      The guessed reference or the provided, qualified, reference
    """
    if reference:
        if not reference.startswith("refs/"):
            return "refs/heads/{}".format(reference)
        return reference

    def _list(*args):
        return execute(rctx, "ls-remote", "--exit-code", remote, git = git, dir = dir, *args)

    result = _list("*")
    if result.return_code != 0:
        fail("Failed to list remote references: `{}`: {}".format(remote, result.stderr))

    pairs = [l.split("\t", 1) for l in result.stdout.strip().splitlines()]

    for sha, reference in pairs:
        if sha == commit:
            return reference

    for _, reference in pairs:
        if reference in guesses:
            return reference

    fail("No common references were available in the remote `{}`: {}. Add a `reference` attribute.".format(remote, ",".join(guesses)))

def _fetch(rctx, *, remote, commit, reference, dir, git):
    """
    Fetches a remote commit into a `git` directory.

    Attempts to do a single depth SHA1 fetch. If the server does not support single SHA1
    fetches, various fallbacks are performed:

    - If a `reference` is provided, keep deepening the fetch until the commit is found
    - If the commit is not found, or a reference is not provided, do a _full_ fetch of the repository

    Args:
      rctx: The repository context to use for command execution.
      remote: The `git` remote endpoint to receive objects from.
      commit: The fully qualified commit SHA.
      reference: A reference that contains the commit SHA.
      dir: The `git` directory to store the object into.
      git: The path to the `git` CLI.

    Returns:
      The execution result.
    """

    def _execute(*args):
        return execute(rctx, "fetch", git = git, dir = dir, *args)

    def _exists(c):
        return execute(rctx, "cat-file", "-e", c, git = git, dir = dir)

    def _warn(fmt, *k, **kw):
        return log(rctx, "fetch", LEVEL.warning, fmt, *k, **kw)

    _warn("Failed to perform a single SHA, shallow fetch of `{}#{}`.".format(remote, commit))

    # Attempt to do a single SHA1 fetch
    result = _execute("--depth=1", remote, commit)
    if result.return_code != 0 and "want {} not valid".format(commit) not in result.stderr:
        return result
    if result.return_code == 0:
        return result

    # Warn the user
    _warn("Failed to perform a single SHA, shallow fetch of `{}#{}`. Falling back to more inefficient methods. The server may not allow advertising of single commits. This can be turned on within the server configuration via `uploadpack.allowReachableSHA1InWant = true`.".format(remote, commit))

    # Guess the reference, if none provided
    reference = _guess(rctx, remote, commit, reference, git = git, dir = dir)

    # Grab a single depth of the reference to be able to deepen
    result = _execute("--depth=1", remote, reference)
    if result.return_code != 0 or _exists(commit):
        return result

    # Deepen using the previous fetch as a negotiation tip
    for shift in range(4, 10):
        depth = 1 << shift
        result = _execute("--negotiation-tip=FETCH_HEAD", "--depth={}".format(depth), remote, reference)
        if result.return_code != 0 or _exists(commit):
            return result

    # Add a more stern warning
    _warn("Performing a full, single branch fetch of `{}#{}`. Make sure the provided reference has the target commit in the first hundred or so commits.".format(remote, commit))

    # Fetch the full depth for the reference
    result = _execute(remote, "{}:{}".format(reference, reference))
    if result.return_code != 0 or _exists(commit):
        return result

    # Help the user improve the fetch with a clear warning
    _warn("Performing a full fetch of `{}#{}` to find the commit. Make sure to provide a relevant reference that provides the commit, the commit was not found in `{}`.".format(remote, commit, reference))

    # Fetch all tags
    result = _execute(remote, "refs/tags/*:refs/tags/*")
    if result.return_code != 0 or _exists(commit):
        return result

    # Fetch all branches
    result = _execute(remote, "refs/heads/*:refs/heads/*")
    if result.return_code != 0 or _exists(commit):
        return result

    fail("""Failed to fetch `{remote}#{commit}` as all fallback methods failed to retrieve the commit.

Fallback methods activate when a `git` server does not support single SHA fetches. This often occurs on Gerrit servers where the ACLs prevent advertising SHA fetches. A workaround in this situation is to push a branch (or tag) for the commit to be retrieved:

    git push origin {commit}:refs/heads/advertise/{commit}

The fetch ultimately falls back to retrieving all tags and branches. It is unexpected not to be able to find the `{commit}` in the repository after a full depth, through fetch. Check that the SHA is valid and exists in the remote repository.
""".format(remote = remote, commit = commit))

def _submodules(rctx, module, *, dir, git):
    """
    Fetches a module and determines the submodules, if any.

    Args:
      rctx: The repository context to use for command execution.
      remote: The `git` remote endpoint to receive objects from.
      commit: The fully qualified commit SHA.
      reference: A reference that contains the commit SHA.
      dir: The `git` directory to store the object into.
      git: The path to the `git` CLI.
      path: The module path.
      name: The module name.

    Returns:
      The submodules of the provided module.
    """
    result = _fetch(
        rctx,
        git = git,
        dir = dir,
        commit = module.commit,
        reference = module.reference,
        remote = module.remote,
    )
    if result.return_code != 0:
        fail("Failed to fetch `{}#{}`: {}".format(module.remote, module.commit, result.stderr))

    head = dir.get_child("HEAD")
    rctx.file(head, module.commit, executable = False)

    refs = dir.get_child("refs")
    rctx.delete(refs)
    rctx.file(refs.get_child(".touch"), "", executable = False)

    return submodules(
        rctx,
        git = git,
        dir = dir,
        commit = module.commit,
        remote = module.remote,
    )

def _modules(rctx, *, remote, commit, reference, dir, git, recursive = True):
    """
    Recursively fetches a remote commit into a `git` directory.

    Args:
      rctx: The repository context to use for command execution.
      remote: The `git` remote endpoint to receive objects from.
      commit: The fully qualified commit SHA.
      reference: A reference that contains the commit SHA.
      dir: The `git` directory to store the object into.
      git: The path to the `git` CLI.
      recursive: Fetch all nested submodules

    Returns:
      The collection of modules fetched.
    """
    _, name = rctx.name.rsplit("~", 1)
    root = struct(
        name = name,
        remote = remote,
        commit = commit,
        reference = reference,
        path = ".",
    )

    modules = [root]

    if not recursive:
        return modules

    for i in range(0, 0x7ee7babe):
        if len(modules) <= i:
            break
        module = modules[i]
        nested = _submodules(rctx, module, git = git, dir = dir)
        kw = structs.to_dict(module)
        kw["nested"] = tuple([m.path for m in nested])
        modules[i] = struct(**kw)
        modules.extend(nested)

    return modules

def fetch(rctx, *, remote = None, commit = None, reference = None, dir = None, git = None, recursive = True):
    """
    Fetches a remote commit into a `git` directory.

    All arguments default to the same named attributes on `rctx.`

    Args:
      rctx: The repository context to use for command execution.
      remote: The `git` remote endpoint to receive objects from.
      commit: The fully qualified commit SHA.
      reference: A reference that contains the commit SHA.
      dir: The `git` directory to store the object into.
      git: The path to the `git` CLI.
      recursive: Fetch all nested submodules

    Returns:
      The collection of modules fetched.
    """
    remote = remote or rctx.attr.remote
    commit = commit or rctx.attr.commit
    reference = reference or rctx.attr.reference
    dir = dir or rctx.attr.dir or rctx.path(".git")
    git = git or rctx.attr.git

    dir = init(rctx, git = git, dir = dir)

    if len(commit) != 40 or not commit.isalnum():
        fail("Can only fetch fully qualified commits: {}".format(commit))

    modules = _modules(rctx, git = git, dir = dir, commit = commit, reference = reference, remote = remote, recursive = recursive)

    head = dir.get_child("HEAD")
    rctx.file(head, commit, executable = False)

    fetch_head = dir.get_child("FETCH_HEAD")
    if fetch_head.exists:
        rctx.delete(fetch_head)

    # Provide a file to make sure the `refs` directory is found in a `filegroup` glob.
    touch = dir.get_child("refs/.touch")
    rctx.file(touch, "", executable = False)

    return modules
