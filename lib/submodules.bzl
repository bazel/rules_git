load(":execute.bzl", "execute")
load("//lib/config:sections.bzl", _sections = "sections")

visibility("//lib/...")

def _commit(rctx, path, *, commit, git, dir):
    def _execute(*args):
        return execute(rctx, git = git, dir = dir, *args)

    result = _execute("ls-tree", "--format=%(objecttype) %(objectname)", commit, path)
    if result.return_code != 0:
        fail("Failed to list submodule tree `{}:{}`: {}".format(commit, path, result.stderr))

    for line in result.stdout.splitlines():
        kind, nested = line.split(" ", 1)

        if kind != "commit":
            continue

        if len(nested) != 40 or not nested.isalnum():
            fail("Invalid submodule commit `{}`: {}".format(nested, result.stdout))

        return nested

    fail("Failed to get submodule commit: {}".format(result.stdout))

def _submodule(rctx, section, *, remote, commit, git, dir):
    if section.name != "submodule":
        fail("Expected section to be a `submodule`: {}".format(section))

    _, _, name = section.quoted.rpartition("/")
    data = dict(section.pairs)

    url = data["url"]
    if url.startswith("."):
        prefix = remote.removesuffix(".git")
        for _ in range(0xBADC0DE):
            if not url.startswith("../"):
                break
            _, url = url.split("/", 1)
            prefix, _ = prefix.rsplit("/", 1)
        url = "{}/{}".format(prefix, url)

    if "path" in data:
        path = data["path"]
    elif url.startswith("git@"):
        _, path = url.removesuffix(".git").split(":", 1)
    elif url.startswith(("https://", "http://")):
        _, path = url.removesuffix(".git").split("://", 1)
        _, path = path.split("/", 1)
    else:
        fail("Unsupported submodule remote: {}".format(url))

    branch = None
    if "branch" in data:
        branch = data["branch"]

    return struct(
        name = name,
        remote = url,
        commit = _commit(rctx, path, commit = commit, git = git, dir = dir),
        reference = branch,
        path = path,
    )

def submodules(rctx, *, remote, commit, dir, git):
    """
    Determines the submodules for a fetched commit.

    Args:
      rctx: The repository context to use for command execution.
      remote: The `git` remote endpoint to receive objects from.
      commit: The fully qualified commit SHA.
      dir: The `git` directory to store the object into.
      git: The path to the `git` CLI.

    Returns:
      The collection of the submodules.
    """

    def _execute(*args):
        return execute(rctx, git = git, dir = dir, *args)

    result = _execute("cat-file", "-e", "{}:.gitmodules".format(commit))
    if result.return_code != 0:
        return ()

    result = _execute("show", "{}:.gitmodules".format(commit))
    if result.return_code != 0:
        fail("Failed to retrieve submodules `{}`: {}".format(commit, result.stderr))

    return tuple([
        _submodule(rctx, s, commit = commit, remote = remote, git = git, dir = dir)
        for s in _sections(result.stdout)
    ])
