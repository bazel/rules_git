visibility("//...")

def execute(rctx, *args, git, dir = None):
    """
    Executes a `git` command with the CLI.

    Args:
      rctx: The repository context to use for command execution.
      *args: Arguments to pass to the CLI
      dir: The (optional) `git` directory to store the object into.
      git: The path to the `git` CLI.

    Returns:
      The execution result.
    """
    cmd = (git,)

    if dir != None:
        cmd += ("--git-dir={}".format(dir),)

    cmd += tuple(args)

    env = {
        "GIT_CONFIG_NOSYSTEM": "true",
    }

    rctx.report_progress(" ".join(args))
    return rctx.execute(cmd, environment = env)
