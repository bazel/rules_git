load("//lib:init.bzl", _INIT = "ATTRS", _init = "init")
load("//lib:resolve.bzl", _RESOLVE = "ATTRS", _resolve = "resolve")
load("//lib:fetch.bzl", _FETCH = "ATTRS", _fetch = "fetch")
load("//git/fetch/dir:template.bzl", _DIR = "ATTRS", _dir = "template")
load("//git/fetch/module:template.bzl", _MODULE = "ATTRS", _module = "template")

visibility("//...")

DOC = """Fetches a `git` repository.

Creates a bare repository in the Bazel repository directory.

The bare repository can be used with subsequent rules to checkout the worktree, convert to an archive, etc.

This rule performs the network access to retrieve the necessary objects. Once retrieved other rules perform the more expensive I/O. This separation of concerns allows moving some of those rules into the execution phase.
"""

ATTRS = {k: v for k, v in (_INIT | _RESOLVE | _FETCH | _MODULE | _DIR).items() if k not in ("dir", "bare")}

def implementation(rctx):
    canonical = {a: getattr(rctx.attr, a) for a in ATTRS} | {"name": rctx.name}

    dir = _init(rctx, dir = rctx.path(".git"), bare = True)

    commit, reference = _resolve(rctx)
    canonical["commit"] = commit
    canonical["reference"] = reference

    modules = _fetch(rctx, commit = commit, dir = dir, reference = reference)

    _dir(rctx, dir = dir)

    for module in modules:
        _module(rctx, module = module)

    return canonical

fetch = repository_rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
)
