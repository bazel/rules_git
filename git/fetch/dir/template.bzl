visibility("//git/fetch/...")

ATTRS = {
    "_dir": attr.label(
        doc = "The `.git` directory `BUILD.bazel` template file.",
        default = ":BUILD.tmpl.bazel",
        allow_single_file = True,
    ),
}

def template(rctx, *, dir, template = None):
    """
    Templates `BUILD.bazel` for the `.git` directory.

    All arguments default to the same named attributes on `rctx.`

    Args:
      rctx: The repository context to use for command execution.
      template: The `BUILD.bazel` template file.

    Returns:
      The templated filepath.
    """
    template = template or rctx.attr._dir

    rendered = dir.get_child("BUILD.bazel")

    substitutions = {}

    rctx.template(rendered, template, substitutions, executable = False)

    return rendered
