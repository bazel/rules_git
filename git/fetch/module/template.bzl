visibility("//git/fetch/...")

ATTRS = {
    "_module": attr.label(
        doc = "The `BUILD.bazel` template file.",
        default = ":BUILD.tmpl.bazel",
        allow_single_file = True,
    ),
}

def template(rctx, *, module, template = None):
    """
    Templates `BUILD.bazel` for the `git_fetch` repository.

    All arguments default to the same named attributes on `rctx.`

    Args:
      rctx: The repository context to use for command execution.
      template: The `BUILD.bazel` template file.
      module: The module that has been fetched into this repository.

    Returns:
      The templated filepath.
    """
    template = template or rctx.attr._module

    path = rctx.path(module.path)

    rendered = path.get_child("BUILD.bazel")

    _, name = rctx.name.rsplit("~", 1)

    substitutions = {
        "{{name}}": module.name,
        "{{remote}}": module.remote,
        "{{commit}}": module.commit,
        "{{reference}}": module.reference or "",
        "{{path}}": module.path,
        "{{deps}}": repr([
            "@{}//{}".format(name, m)
            for m in module.nested
        ]),
    }

    rctx.template(rendered, template, substitutions, executable = False)

    return rendered
