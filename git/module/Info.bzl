load("@bazel_skylib//lib:types.bzl", "types")

visibility("//git/module/...")

def _is_hex(value):
    for c in value.elems():
        if not (("A" <= c and c <= "F") or ("a" <= c and c <= "f") or ("0" <= c and c <= "9")):
            return False
    return True

def init(name, remote, commit, path, reference = None):
    """
    Initializes an `Info` provider.

    Args:
      name: The name of the module
      remote: Upstream repository the module was fetched from
      commit: The commit SHA that was fetched
      path: The relative path of the module
      reference: The reference that was fetched (optional)

    Returns:
      A mapping of keywords for the `info` raw constructor.
    """

    # Check types
    if not types.is_string(name):
        fail("`GitModuleInfo.name` must be a `str`: {}".format(name))

    if not types.is_string(remote):
        fail("`GitModuleInfo.remote` must be a `str`: {}".format(remote))

    if not types.is_string(commit):
        fail("`GitModuleInfo.commit` must be a `str`: {}".format(commit))

    if not types.is_string(path):
        fail("`GitModuleInfo.path` must be a `str`: {}".format(path))

    if reference and not types.is_string(reference):
        fail("`GitModuleInfo.reference` must be a `str`: {}".format(reference))

    # Check for empty
    if not name:
        fail("`GitModuleInfo.name` must not be empty")

    if not remote:
        fail("`GitModuleInfo.remote` must not be empty")

    if not commit:
        fail("`GitModuleInfo.commit` must not be empty")

    if not path:
        fail("`GitModuleInfo.path` must not be empty")

    # Check SHA
    if not _is_hex(commit):
        fail("`GitModuleInfo.commit` must be hexidecimal: {}".format(commit))

    if len(commit) not in (40, 64):
        fail("`GitModuleInfo.commit` must be of length 40 or 64: {}".format(commit))

    return {
        "name": name,
        "remote": remote,
        "commit": commit.lower(),
        "reference": reference or None,
        "path": path,
    }

GitModuleInfo, git_module_info = provider(
    "Information about a `git` module.",
    fields = ["name", "remote", "commit", "reference", "path"],
    init = init,
)

Info = GitModuleInfo
info = git_module_info
