load(":Info.bzl", "GitModuleInfo")
load("//git/modules:Info.bzl", "GitModulesInfo")

visibility("//...")

DOC = """Provides a information about a fetch `git` module."""

ATTRS = {
    "remote": attr.string(
        doc = "The remote that the `git` objects were fetched from.",
        mandatory = True,
    ),
    "commit": attr.string(
        doc = "The commit SHA.",
        mandatory = True,
    ),
    "path": attr.string(
        doc = "The relative path for the `git` module.",
        mandatory = True,
    ),
    "reference": attr.string(
        doc = "The (optional) reference that contained the commit.",
    ),
    "deps": attr.label_list(
        doc = "Nested submodules.",
    ),
}

def _modules(module, deps):
    modules = [d[GitModulesInfo].modules for d in deps]
    return depset([module], transitive = modules)

def _files(file, deps):
    files = [d[GitModulesInfo].files for d in deps]
    return depset([file], transitive = files)

def implementation(ctx):
    module = GitModuleInfo(
        name = ctx.label.name,
        remote = ctx.attr.remote,
        commit = ctx.attr.commit,
        path = ctx.attr.path,
        reference = ctx.attr.reference or None,
    )

    output = ctx.actions.declare_file("{}.json".format(ctx.label.name))

    ctx.actions.write(
        output = output,
        content = json.encode(module),
        is_executable = False,
    )

    modules = GitModulesInfo(
        modules = _modules(module, ctx.attr.deps),
        files = _files(output, ctx.attr.deps),
    )

    default = DefaultInfo(files = modules.files)

    return [module, modules, default]

git_module = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    provides = [GitModuleInfo, GitModulesInfo, DefaultInfo],
)

module = git_module
