#! /usr/bin/env bash

# Strict Shell
set -o errexit
set -o nounset
set -o pipefail

# Bazel replacements
SORT="{{sort}}"
CP="{{cp}}"
CHMOD="{{chmod}}"
LN="{{ln}}"
MKDIR="{{mkdir}}"
GIT="{{git}}"
readonly SORT CP CHMOD LN MKDIR GIT

# Parse arguments
while test 0 -ne "${#}"; do
  case "${1}" in
  "--git-dir")
    shift
    GITDIR="${1?Must provide an argument for --git-dir}"
    ;;
  "--work-tree")
    shift
    WORKTREE="${1?Must provide an argument for --work-tree}"
    ;;
  "--force")
    FORCE="--force"
    ;;
  "checkout")
    shift
    break
    ;;
  *)
    printf >&2 'Error: unknown argument: %s\n' "${1}"
    exit 2
    ;;
  esac
  shift
done
readonly GITDIR WORKTREE
if test "${#}" -eq 0; then
  echo >&2 "Must provide modules to checkout"
  exit 2
fi

# The `git` CLI has strict requirements for the `.git` directory[1]
# Specifically:
#   * `HEAD` if a symlink _must_ point to a file in the `refs` directory
#   * `HEAD` must be writable
#   * `objects`/`refs _must_ exist
# [1]: https://github.com/git/git/blob/21306a098c3f174ad4c2a5cddb9069ee27a548b0/setup.c#L355
DST=".git"
readonly DST
"${MKDIR}" -p "${DST}"
for SRC in "${GITDIR}/"*; do
  case "${SRC}" in
  *"/HEAD")
    "${CP}" -p "${SRC}" "${DST}/${SRC#"${GITDIR}"/}"
    "${CHMOD}" u+w "${DST}/${SRC#"${GITDIR}"/}"
    ;;
  *"/objects" | *"/refs") # TODO: can we symlink here?
    "${CP}" -RLp "${SRC}" "${DST}/${SRC#"${GITDIR}"/}"
    ;;
  *)
    "${LN}" -s "${SRC}" "${DST}/${SRC#"${GITDIR}"/}"
    ;;
  esac
done

# Do the checkout of each module
(for MODULE in "${@}"; do echo "${MODULE}"; done) |
  "${SORT}" -t ":" |
  while IFS=: read -r RELATIVE COMMIT; do
    # shellcheck disable=SC2086
    "${GIT}" \
      --git-dir "${DST}" \
      --work-tree "${WORKTREE}/${RELATIVE}" \
      checkout \
      ${FORCE-} \
      "${COMMIT}"
  done
