load("@bazel_skylib//lib:types.bzl", "types")

visibility("//git/module/...")

def init(modules, files):
    """
    Initializes an `GitModulesInfo` provider.

    Args:
      modules: The dependency set of `git` modules.
      files: The rendered JSON files.

    Returns:
      A mapping of keywords for the `info` raw constructor.
    """

    # Check types
    if not types.is_depset(modules):
        fail("`GitModulesInfo.modules` must be a `depset`: {}".format(modules))

    if not types.is_depset(files):
        fail("`GitModulesInfo.files` must be a `depset`: {}".format(files))

    return {
        "modules": modules,
        "files": files,
    }

GitModulesInfo, git_modules_info = provider(
    "Information about `git` modules.",
    fields = ["modules", "files"],
    init = init,
)

Info = GitModulesInfo
info = git_modules_info
