load("//git/modules:defs.bzl", "GitModulesInfo")

visibility("//...")

DOC = """Checks out a working tree from a fetched repository."""

ATTRS = {
    "dir": attr.label(
        doc = "`git` directory to checkout `commit`.",
        mandatory = True,
    ),
    "module": attr.label(
        doc = "The commit SHA to archive the `objects`.",
        providers = [GitModulesInfo],
        mandatory = True,
    ),
    "format": attr.string(
        doc = "The format of the archive",
        values = ["tar"],
        default = "tar",
    ),
    "compress": attr.label(
        doc = "A compression binary.",
        default = "//git/archive/compress:cat",
        executable = True,
        cfg = "exec",
    ),
    "prefix": attr.string(
        doc = "A path prefix to for the archive.",
        default = "./",
    ),
    "_template": attr.label(
        doc = "The script to template and run.",
        allow_single_file = True,
        default = ":template",
    ),
}

def _find_head(file):
    if file.basename == "HEAD":
        return file.dirname
    return None

def _module(module):
    return "{}:{}".format(module.path, module.commit)

def implementation(ctx):
    concatenate = ctx.toolchains["@rules_tar//tar/toolchain/concatenate:type"]
    sort = ctx.toolchains["@rules_coreutils//coreutils/toolchain/sort:type"]
    chmod = ctx.toolchains["@rules_coreutils//coreutils/toolchain/chmod:type"]
    cp = ctx.toolchains["@rules_coreutils//coreutils/toolchain/cp:type"]
    ln = ctx.toolchains["@rules_coreutils//coreutils/toolchain/ln:type"]
    mkdir = ctx.toolchains["@rules_coreutils//coreutils/toolchain/mkdir:type"]
    git = ctx.toolchains["//git/toolchain/git:type"]

    rendered = ctx.actions.declare_file("{}.rendered.{}".format(ctx.label.name, ctx.file._template.extension))
    substitutions = ctx.actions.template_dict()

    # TODO: remove `.path` when `TemplateDict#add` supports `File` for Path Mapping
    substitutions.add("{{concatenate}}", concatenate.executable.path)
    substitutions.add("{{sort}}", sort.executable.path)
    substitutions.add("{{chmod}}", chmod.executable.path)
    substitutions.add("{{cp}}", cp.executable.path)
    substitutions.add("{{ln}}", ln.executable.path)
    substitutions.add("{{mkdir}}", mkdir.executable.path)
    substitutions.add("{{compress}}", ctx.executable.compress.path)
    substitutions.add("{{git}}", git.executable.path)
    ctx.actions.expand_template(
        output = rendered,
        template = ctx.file._template,
        computed_substitutions = substitutions,
        is_executable = True,
    )

    modules = ctx.attr.module[GitModulesInfo].modules

    archive = ctx.actions.declare_file("{}".format(ctx.label.name))

    args = ctx.actions.args()
    args.add_joined("--git-dir", ctx.files.dir, map_each = _find_head, join_with = ",", omit_if_empty = False)
    args.add("archive")
    args.add("--format").add(ctx.attr.format)
    args.add("--prefix").add(ctx.attr.prefix)
    args.add("--output").add(archive)
    args.add_all(modules, map_each = _module)

    ctx.actions.run(
        outputs = [archive],
        inputs = ctx.files.dir,
        executable = rendered,
        tools = [
            concatenate.run,
            sort.run,
            chmod.run,
            cp.run,
            ln.run,
            mkdir.run,
            git.run,
            ctx.attr.compress.files_to_run,
        ],
        arguments = [args],
        mnemonic = "GitArchive",
    )

    return DefaultInfo(files = depset([archive]))

git_archive = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = [
        "//git/toolchain/git:type",
        "@rules_coreutils//coreutils/toolchain/sort:type",
        "@rules_coreutils//coreutils/toolchain/chmod:type",
        "@rules_coreutils//coreutils/toolchain/cp:type",
        "@rules_coreutils//coreutils/toolchain/ln:type",
        "@rules_coreutils//coreutils/toolchain/mkdir:type",
        "@rules_tar//tar/toolchain/concatenate:type",
    ],
)

archive = git_archive
