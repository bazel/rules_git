#! /usr/bin/env bash

# Strict Shell
set -o errexit
set -o nounset
set -o pipefail

# Bazel replacements
CONCATENATE="{{concatenate}}"
SORT="{{sort}}"
CP="{{cp}}"
CHMOD="{{chmod}}"
LN="{{ln}}"
MKDIR="{{mkdir}}"
COMPRESS="{{compress}}"
GIT="{{git}}"
readonly CONCATENATE SORT CP CHMOD LN MKDIR COMPRESS GIT

# Parse arguments
while test 0 -ne "${#}"; do
  case "${1}" in
  "--git-dir")
    shift
    GITDIR="${1?Must provide an argument for --git-dir}"
    ;;
  "archive") ;;
  "--format")
    shift
    FORMAT="${1?Must provide an argument for --format}"
    ;;
  "--prefix")
    shift
    PREFIX="${1?Must provide an argument for --prefix}"
    ;;
  "--output")
    shift
    OUTPUT="${1?Must provide an argument for --output}"
    shift
    break
    ;;
  *)
    printf >&2 'Error: unknown argument: %s\n' "${1}"
    exit 2
    ;;
  esac
  shift
done
readonly GITDIR FORMAT OUTPUT PREFIX
if test "${#}" -eq 0; then
  echo >&2 "Must provide modules to archive"
  exit 2
fi

# The `git` CLI has strict requirements for the `.git` directory[1]
# Specifically:
#   * `HEAD` if a symlink _must_ point to a file in the `refs` directory
#   * `HEAD` must be writable
#   * `objects`/`refs _must_ exist
# [1]: https://github.com/git/git/blob/21306a098c3f174ad4c2a5cddb9069ee27a548b0/setup.c#L355
DST="${OUTPUT}.working"
readonly DST
"${MKDIR}" -p "${DST}"
for SRC in "${GITDIR}/"*; do
  case "${SRC}" in
  *"/HEAD")
    "${CP}" -p "${SRC}" "${DST}/${SRC#"${GITDIR}"/}"
    "${CHMOD}" u+w "${DST}/${SRC#"${GITDIR}"/}"
    ;;
  *"/objects" | *"/refs") # TODO: can we symlink here?
    "${CP}" -RLp "${SRC}" "${DST}/${SRC#"${GITDIR}"/}"
    ;;
  *)
    "${LN}" -s "${SRC}" "${DST}/${SRC#"${GITDIR}"/}"
    ;;
  esac
done

# Resolve a path
prefix() (
  FILEPATH="${1}"
  while test "${FILEPATH#*/./}" != "${FILEPATH}"; do
    FILEPATH="${FILEPATH%%/./*}/${FILEPATH#*/./}"
  done
  while test "${FILEPATH#./}" != "${FILEPATH}"; do
    FILEPATH="${FILEPATH#./}"
  done
  if test "${FILEPATH}" = "./"; then
    return
  fi
  echo "--prefix=${FILEPATH}"
)

# Do the archive of the module
{
  (for MODULE in "${@}"; do echo "${MODULE}"; done) |
    "${SORT}" -t ":" |
    while IFS=: read -r RELATIVE COMMIT; do
      FLAG="$(prefix "${PREFIX-./}${RELATIVE}/")"
      # shellcheck disable=SC2086
      "${GIT}" \
        --git-dir "${DST}" \
        archive \
        --format="${FORMAT}" \
        ${FLAG} \
        "${COMMIT}"
    done
} |
  "${CONCATENATE}" --duplicate skip |
  "${COMPRESS}" >"${OUTPUT}"
