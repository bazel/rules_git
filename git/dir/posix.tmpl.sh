#! /usr/bin/env sh

# Strict Shell
set -o errexit
set -o nounset

# Bazel replacements
CP="{{cp}}"
readonly CP

# Process arguments
DST="${1?Must provide a destination directory}"
readonly DST
shift

# Ensure the directory exists
test -d "${DST}"

# Copy the `git` directories
for FILEPATH in "${@}"; do
  for SRC in "${FILEPATH}/"*; do
    "${CP}" -RLp "${SRC}" "${DST}"
  done
done
