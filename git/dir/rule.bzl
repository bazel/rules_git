visibility("//...")

DOC = """Creates a `git` declared directory from a filegroup.

Specifically, it makes sure that the `HEAD` passes the `git` CLI validation.
"""

ATTRS = {
    "srcs": attr.label_list(
        doc = "`git` directory files.",
        mandatory = True,
    ),
    "template": attr.label(
        doc = "The script to template and run.",
        allow_single_file = True,
        default = ":template",
    ),
}

def _map_each(file):
    if file.basename == "HEAD":
        return file.dirname
    return None

def implementation(ctx):
    cp = ctx.toolchains["@rules_coreutils//coreutils/toolchain/cp:type"]

    rendered = ctx.actions.declare_file("{}.rendered.{}".format(ctx.label.name, ctx.file.template.extension))
    ctx.actions.expand_template(
        output = rendered,
        template = ctx.file.template,
        substitutions = {
            "{{cp}}": cp.executable.path,
        },
        is_executable = True,
    )

    dir = ctx.actions.declare_directory("{}.git".format(ctx.label.name))
    args = ctx.actions.args()
    args.add(dir.path)
    args.add_all(ctx.files.srcs, map_each = _map_each)

    ctx.actions.run(
        outputs = [dir],
        inputs = ctx.files.srcs,
        executable = rendered,
        tools = [cp.run],
        arguments = [args],
        mnemonic = "GitDir",
        progress_message = "Creating `.git` directory: %{output}",
    )

    return DefaultInfo(files = depset([dir]))

dir = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = [
        "@rules_coreutils//coreutils/toolchain/cp:type",
    ],
)
